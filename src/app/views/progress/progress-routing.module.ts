import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProgressComponent } from './progress.component';

const routes: Routes = [
  {
    path: '',
    component: ProgressComponent,
    data: {
      title: 'menu'
    }
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgressRoutingModule { }
