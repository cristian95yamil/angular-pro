import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { CommonModule } from '@angular/common';


import { ProgressRoutingModule } from './progress-routing.module';
import { ProgressComponent } from './progress.component';
import { IncrementadorComponent } from '../../components/incrementador/incrementador.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ProgressRoutingModule,

  ],
  declarations: [
    ProgressComponent,
    IncrementadorComponent

  ]
})
export class ProgressModule { }
