import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../models/usuario.model';
import { UsuarioService } from '../../services/service.index';

import swal from 'sweetalert2';
import { SubirArchivoService } from '../../services/subir-archivo/subir-archivo.service';
import { ModalUploadService } from './modal-upload.service';


@Component({
  selector: 'app-modal-upload',
  templateUrl: './modal-upload.component.html',
  styles: []
})
export class ModalUploadComponent implements OnInit {
  usuario: Usuario;
  imagenSubir: File;
  imagenTemp: string | ArrayBuffer;
  oculto: String = '';
  constructor(
    public _usuarioService: UsuarioService,
    public _modalUploadService: ModalUploadService,
    public _subirArchivoService: SubirArchivoService
  ) {
  }

  ngOnInit() {
    this.getPersona();
  }
  getPersona() {
    console.log('modal listo')
  }

  changeImage(usuario: Usuario) {
    this._usuarioService.changeImage(this.imagenSubir, usuario._id);
  }

  /**
  * Evento se dispara cada que seleciona una imagen
  * recive un archivo tipo file
  */
  selectionImagen(archivo: any) {
    if (!archivo) {
      this.imagenSubir = null;
      return;
    }
    if (archivo.type.indexOf('image') < 0) {
      swal('Solo imágenes', 'El archivo seleccionado no es una imagen', 'error');
      this.imagenSubir = null;
    }
    this.imagenSubir = archivo;

    let reader = new FileReader();

    let urlImgenTemp = reader.readAsDataURL(archivo);

    reader.onloadend = () => {
      this.imagenTemp =  reader.result;
      console.log(reader.result)
    }
    console.log(archivo)
  }
  cloceModal() {
    this.imagenTemp = null;
    this.imagenSubir = null;
    this._modalUploadService.cloceModal();
  }
  sendFile() {
    this._subirArchivoService.uploadFile(this.imagenSubir, this._modalUploadService.tipo, this._modalUploadService.id)
      .then(resp => {
        console.log(resp);
        this._modalUploadService.notificacion.emit(resp);
        this.cloceModal();
        this._modalUploadService.cloceModal();
      })
      .catch(resp => {
        console.log('Error en la carga ..');
      })
  }
}
