import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { UsuarioService } from '../services/usuario/usuario.service';
import { Usuario } from '../models/usuario.model';


declare const gapi: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: []
})
export class LoginComponent implements OnInit {

  public auth2: any;
  recuerdame = false;
  email: string;
  constructor(
    public _usuarioService: UsuarioService,
    public router: Router) { }

    ngOnInit() {
      this.email = localStorage.getItem('email') || '';
      if ( this.email.length > 1) {
        this.recuerdame = true;
      }
      this.googleInit();
    }

    googleInit() {
      gapi.load('auth2', () => {
        this.auth2 = gapi.auth2.init({
          client_id: '313026793888-7psdqvki15djdsgug0dqv0g5td9413rn.apps.googleusercontent.com',
          cookiepolicy: 'single_host_origin',
          scope: 'profile email'
        });
        this.attachSignin(document.getElementById('googleBtn'));
      });
    }

    attachSignin(element) {
      this.auth2.attachClickHandler(element, {},
        (googleUser) => {
          // tslint:disable-next-line:prefer-const
          let profile = googleUser.getBasicProfile();
          console.log('Token || ' + googleUser.getAuthResponse().id_token);
          let token = googleUser.getAuthResponse().id_token;
          this._usuarioService.loginWhitGoogle( token )
            .subscribe( resp  => {
              console.log(resp);
              // this.router.navigate(['/dashboard']);
              window.location.href = '#/menu';
            })
          // console.log('ID: ' + profile.getId());
          // console.log('Name: ' + profile.getName());
          // console.log('Image URL: ' + profile.getImageUrl());
          // console.log('Email: ' + profile.getEmail());
          // YOUR CODE HERE
        }, (error) => {
          alert(JSON.stringify(error, undefined, 2));
        });
    }

  ingresar(forma: NgForm) {

    if (forma.invalid) {
      return
    }

    const usuario = new Usuario(null, forma.value.email, forma.value.password );

    this._usuarioService.login(usuario, this.recuerdame)
    .subscribe( resp => {
      this.router.navigate(['/menu']);
    })
  }

}
