import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  ValidatorFn,
  AbstractControl,
  FormBuilder
} from '@angular/forms';

import swal from 'sweetalert2';
import { UsuarioService } from '../services/service.index';
import { Usuario } from '../models/usuario.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit {
  forma: FormGroup;
  private RegistrationForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    public router: Router,
    public _usuarioService: UsuarioService) {}

  sonIguales(campo1: string, campo2: string) {
    return (group: FormGroup) => {
      // tslint:disable-next-line:prefer-const
      let pass1 = group.controls[campo1].value;
      // tslint:disable-next-line:prefer-const
      let pass2 = group.controls[campo2].value;

      if (pass1 === pass2) {
        return null;
      }
      return { sonIguales: true}
    };
  }
  ngOnInit() {
    this.forma = this.fb.group(
      {
        nombre: new FormControl(null, Validators.required),
        email: new FormControl(null, [Validators.required, Validators.email]),
        password: new FormControl(null, Validators.required),
        password2: new FormControl(null, Validators.required),
        condiciones: new FormControl(false)
      },
      { validator: this.sonIguales('password' , 'password2') } );

      this.forma.setValue({
        nombre: 'cristian',
        email: 'tt@gmail.com',
        password: '1234',
        password2: '1234',
        condiciones: true
      })
  }

  registerUser() {
    if (this.forma.invalid) {
      return;
    }
    if (!this.forma.value.condiciones) {
      swal({
        title: 'Importante!',
        text: 'Debe aceptar las condiciones',
        type: 'warning',
        confirmButtonText: 'ok'
      })
      console.log('Debe de aceptar las condiciones');
    }
    const usuario = new Usuario(
      this.forma.value.nombre,
      this.forma.value.email,
      this.forma.value.password,
    );
    this._usuarioService.registerUser(usuario)
      .subscribe( resp => {
        this.router.navigate(['/login']);
        console.log(resp);
      })
    console.log(this.forma.value);
    console.log('forma' + this.forma.valid);
  }
}
