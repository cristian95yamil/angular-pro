import { Component, OnInit } from '@angular/core';
import { HospitalesService } from '../../services/hospital/hospitales.service';
import { Hospital } from '../../models/hospital.model';
import swal from 'sweetalert2';
import { UsuarioService } from '../../services/usuario/usuario.service';
import { ModalUploadService } from '../../components/modal-upload/modal-upload.service';

@Component({
  selector: 'app-hospitales',
  templateUrl: './hospitales.component.html',
  styles: []
})
export class HospitalesComponent implements OnInit {
  hospitales: Hospital[] = [];
  paginacion = 20;
  page = 1;
  desde: number = 0;
  totalRegistros: number = 5;

  cargando: boolean = false;
  constructor(
    public _hospitalService: HospitalesService,
    public _modalUploadService:  ModalUploadService
  ) { }

  ngOnInit() {
    this.getHospitales();
    this._modalUploadService.notificacion
      .subscribe( () => this.getHospitales());
  }
  searchHospital(search: string) {
    console.log(search);
    if ( search.length <= 0 ) {
      this.getHospitales();
      this.cargando = true;
      return;
    }
    this._hospitalService.searchHospital(search)
      .subscribe((resp: any) => {
        this.hospitales = resp;
        this.cargando = true;
        this.paginacion = (this._hospitalService.totalHospitales / 5) * 10;

      });
    this.cargando = false;
  }
  getHospitales() {
    this._hospitalService.getHospitales(this.desde)
    .subscribe( (resp: any) => {
      console.log(resp)
      this.hospitales = resp;
      this.totalRegistros = this._hospitalService.totalHospitales;
      this.paginacion = (this.totalRegistros / 5) * 10;
      this.cargando = true;
    } );
  }
  newHospital() {
    swal.mixin({
      input: 'text',
      confirmButtonText: 'Guardar',
      showCancelButton: true,
      progressSteps: ['1']
    }).queue([
      {
        title: 'Crear nuevo Hospital',
        text: 'Ingrese nombre Hospital a crear'
      },

    ]).then((result) => {
      if (result.value) {
        this._hospitalService.newHospital(result.value)
          .subscribe( resp => this.getHospitales()
        )
        swal({
          title: 'El Hopital ha sido creado correctamente !!!',
          confirmButtonText: 'Exito!'
        })
      }
    })
  }
  next(valor: number) {
    valor -= 1;
    valor = valor * 5;
    if (valor <= 0) {
      this.desde = 0;
      this.getHospitales();
    } else {
      this.desde = valor;
      this.getHospitales();
    }
  }
  deletedHospital(hospital: Hospital) {
    console.log(hospital._id)
    swal({
      title: 'Estas seguro de borrar este Hospital?',
      text: 'No podras revertir este paso despues',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si borrar'
    }).then((result) => {
      if (result.value) {
        this._hospitalService.deleteHospital(hospital._id)
        .subscribe( resp => {
          console.log(resp)
          this.getHospitales();
        })
        swal(
          'Borrado!',
          'Hospital ha sido borrado correctamente.',
          'success'
        )
      }
    })

  }
  updateHospital(hospital: Hospital) {
    console.log(hospital.nombre)
    this._hospitalService.updateHospital(hospital)
      .subscribe(resp => {
        console.log('actualizando hospital')
        }
      )
  }

  viewModal(id: string) {
    console.log(id)
    this._modalUploadService.viewModal('hospitales', id);
  }
}
