import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProgressComponent } from './progress/progress.component';
import { AccoutSettingsComponent } from './accout-settings/accout-settings.component';
import { PromesasComponent } from './promesas/promesas.component';
import { RxjsComponent } from './rxjs/rxjs.component';
import { LoginGuardGuard } from '../services/guards/login-guard.guard';
import { ProfileComponent } from './profile/profile.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { MedicosComponent } from './medicos/medicos.component';
import { HospitalesComponent } from './hospitales/hospitales.component';
import { MedicoComponent } from './medicos/medico.component';
import { BusquedaComponent } from './busqueda/busqueda.component';
import { AdminGuard } from '../services/guards/admin.guard';

const routes: Routes = [
  {
    path: '',
    canActivate: [ LoginGuardGuard],
    data: {
      title: 'menu'
    },
    children: [
      {
        path: 'progreso',
        component: ProgressComponent,
        data: {
          title: 'Buttons'
        }
      },
      {
         path: 'account-settings',
         component: AccoutSettingsComponent,
         data: {
           title: 'Configuration Settings'
         }
      },
      {
        path: 'promesas',
        component: PromesasComponent,
        data: {
          title: 'Promesa as'
        },
      },
      {
        path: 'rxjs',
        component: RxjsComponent,
        data: {
          title: 'Promesas Rxjs'
        },
      },
      // Mantenimientos
      {
        path: 'perfil',
        component: ProfileComponent,
        data: {
          title: 'Perfil de Usuario'
        },
      },
      {
        path: 'usuario',
        // admin guard bloquea la ruta si no es un usuario administrador
        canActivate: [ AdminGuard],
        component: UsuariosComponent,
        data: {
          title: 'Usuario'
        },
      },
      {
        path: 'medicos',
        component: MedicosComponent,
        data: {
          title: 'medicos'
        },
      },
      {
        path: 'medicos/nuevo/:id',
        component: MedicoComponent,
        data: {
          title: 'nuevo'
        },
      },
      {
        path: 'hospitales',
        component: HospitalesComponent,
        data: {
          title: 'hospitales'
        },
      },
    ]
  },
  {
    path: 'busqueda/:termino',
    canActivate: [ LoginGuardGuard],
    component: BusquedaComponent,
    data: {
      title: 'Busqueda'
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
