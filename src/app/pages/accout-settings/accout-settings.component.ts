import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { SettingsService } from '../../services/service.index';

@Component({
  selector: 'app-accout-settings',
  templateUrl: './accout-settings.component.html',
  styles: []
})
export class AccoutSettingsComponent implements OnInit {

  tema: any;
  constructor(
              @Inject(DOCUMENT) private _document,
              public _service: SettingsService
            ) { }

  ngOnInit() {
    this._service.cargarAjustes();
  }

  cambiarColor( color: string ) {
    console.log(color);
    this.tema = this._document.getElementById(this._service.ajuste.id);
    this._service.guardarAjustes(color);
    this._service.ajuste.color = this.tema.style.background = color;
    console.log(this._document.getElementById('tema').set);
  }
}
