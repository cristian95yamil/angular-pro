import { Component, OnInit } from '@angular/core';
import { MedicoService } from '../../services/medico/medico.service';
import { Medico } from '../../models/medico.model';
import swal from 'sweetalert2';
import { ModalUploadService } from '../../components/modal-upload/modal-upload.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-medicos',
  templateUrl: './medicos.component.html',
  styles: []
})
export class MedicosComponent implements OnInit {
  medicos: Medico[] = [];
  paginacion = 20;
  page = 1;
  desde: number = 0;
  totalRegistros: number = 5;

  cargando: boolean = false;
  constructor(
    public _medicoService: MedicoService,
    public _modalUploadService: ModalUploadService,
    public router: Router
  ) { }

  ngOnInit() {
    console.log(this._medicoService);
    this.getMedicos();
    this._modalUploadService.notificacion
      .subscribe(() => this.getMedicos());
  }
  getMedicos() {
    this._medicoService.getMedico(this.desde)
      .subscribe((resp: any) => {
        console.log(resp)
        this.medicos = resp;
        this.totalRegistros = this._medicoService.totalHospitales;
        this.paginacion = (this.totalRegistros / 5) * 10;
        this.cargando = true;
      });
  }

  next(valor: number) {
    valor -= 1;
    valor = valor * 5;
    if (valor <= 0) {
      this.desde = 0;
      this.getMedicos();
    } else {
      this.desde = valor;
      this.getMedicos();
    }
  }

  searchMedico(search: string) {
    console.log(search);
    if (search.length <= 0) {
      this.getMedicos();
      this.cargando = true;
      return;
    }
    this._medicoService.searchMedico(search)
      .subscribe((resp: any) => {
        this.medicos = resp;
        this.cargando = true;
        this.paginacion = (this._medicoService.totalHospitales / 5) * 10;

      });
    this.cargando = false;
  }
  newMedico() {
    this.router.navigate(['/menu/medicos/nuevo', '']);
  }
  updateMedico(medico: Medico) {
    console.log(medico.nombre)
    this._medicoService.updateMedico(medico)
      .subscribe(resp => {
        console.log('actualizando hospital')
        }
      )
  }
  deletedMedico(medico: Medico) {
    console.log(medico._id)
    swal({
      title: 'Estas seguro de borrar este Hospital?',
      text: 'No podras revertir este paso despues',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si borrar'
    }).then((result) => {
      if (result.value) {
        this._medicoService.deleteMedico(medico._id)
        .subscribe( resp => {
          console.log(resp)
          this.getMedicos();
        })
        swal(
          'Borrado!',
          'Hospital ha sido borrado correctamente.',
          'success'
        )
      }
    })

  }
  viewModal(id: string) {
    console.log(id)
    this._modalUploadService.viewModal('hospitales', id);
  }
  viewMedico(id: string) {
    this.router.navigate(['/menu/medicos/nuevo/', id]);
  }
}
