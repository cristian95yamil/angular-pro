import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Hospital } from '../../models/hospital.model';
import { MedicoService } from '../../services/medico/medico.service';
import { HospitalesService } from '../../services/hospital/hospitales.service';
import { Medico } from '../../models/medico.model';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalUploadService } from '../../components/modal-upload/modal-upload.service';

@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styles: []
})
export class MedicoComponent implements OnInit {

  hospitales: Hospital[] = [];
  medico: Medico = new Medico('', '', null , new Hospital('1'), '');
  hospital: Hospital = new Hospital('');

  constructor(
    public _medicoService: MedicoService,
    public _hospitalService: HospitalesService,
    public router: Router,
    public activateRoouter: ActivatedRoute,
    public _modalUploadService: ModalUploadService
  ) {
    activateRoouter.params.subscribe(params => {
      console.log(params['id'])
      let id = params['id']
      if (id !== '') {
        this.cargarMedico(id);
      }
    })
  }

  ngOnInit() {
    this._hospitalService.getHospitales()
      .subscribe(resp => this.hospitales = resp)

    this._modalUploadService.notificacion
      .subscribe( resp => {
        console.log( resp )
      })
  }

  saveMedico(f: NgForm) {
    console.log('guardando medico' + f.value);
    console.log(f.valid)
    if (f.invalid) {
      return;
    }
    this._medicoService.newMedico(this.medico)
      .subscribe(resp => {
        console.log(resp)
        this.medico = resp;
        this.router.navigate(['/menu/medicos']);

        this.medico = new Medico();
      });
  }
  changeHospital(id: string) {
    console.log(id);
    this._hospitalService.getHospital(id)
      .subscribe(resp => {
        this.hospital = resp;
      })
  }

  cargarMedico(id: string) {
    this._medicoService.cargarMedico(id)
      .subscribe(medico => {
        this.medico = medico
        this.medico.hospital._id = medico.hospital._id;
        this.changeHospital(this.medico.hospital._id);
        console.log(this.medico.hospital._id)
      } )
  }
  viewModal(id: string) {
    console.log(id)
    this._modalUploadService.viewModal('medicos', this.medico._id);
    this.cargarMedico(id);
  }
}
