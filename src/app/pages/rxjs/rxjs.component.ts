import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { connectableObservableDescriptor } from 'rxjs/observable/ConnectableObservable';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styles: []
})
export class RxjsComponent implements OnInit , OnDestroy {

  subscription: Subscription;
  constructor() {

    this.subscription = this.regresarObservable()
    .subscribe(
      numero => console.log( 'Subs', numero),
      error => console.log('Error en el obs ', error ),
      () => console.log('El observador termino !'),
    )
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    console.log('La pagina se va a cerrar');
    this.subscription.unsubscribe();
  }
  regresarObservable(): Observable<any> {
    return new Observable( observer => {
      let contador = 0 ;

      const intervalo = setInterval ( () => {
        contador += 1 ;

        const salida = {
          valor: contador
        }

        observer.next(salida);

        // if (contador === 3 ) {
        //   clearInterval(intervalo);
        //   observer.complete();
        // }
        // if ( contador === 2 ) {
        //   observer.error( 'Error de observable');
        // }

      }, 500)
    } )
    .retry(2)
    .map( (resp: any) => {
      return resp.valor;
    })
    .filter( (valor, index) => {
      if ( (valor % 2) === 1 ) {
        // impar
        return true;
      }else {
        // par
        return false;
      }
    });
  }
}
