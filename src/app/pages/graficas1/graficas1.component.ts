import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-graficas1',
  templateUrl: './graficas1.component.html',
  styles: []
})
export class Graficas1Component implements OnInit {

  graficos: any = {
    'grafico1': {
      'labels': ['con frijoles' , 'anan' , 'persona'],
      'data': [15, 15, 54],
      'type': ['doughnut'],
      'leyenda': 'El pan se come con :'
    },
    'grafico2': {
      'labels': ['con frijoles' , 'anan' , 'patata'],
      'data': [1, 30, 54],
      'type': ['doughnut'],
      'leyenda': '@cristian salazar:'
    },
    'grafico3': {
      'labels': ['con frijoles' , 'anan' , 'persona'],
      'data': [40, 15, 54],
      'type': ['doughnut'],
      'leyenda': 'El pan se come con :'
    },
    'grafico4': {
      'labels': ['con frijoles' , 'anan' , 'persona'],
      'data': [50, 15, 54],
      'type': ['doughnut'],
      'leyenda': 'El pan se come con :'
    }
  }

  constructor() { }

  ngOnInit() {
  }

}
