import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { Graficas1RoutingModule } from './graficas1-routing.module';
import { Graficas1Component } from './graficas1.component';

import { ChartsModule } from 'ng2-charts/ng2-charts';
import { GraficoDonaComponent } from '../../components/grafico-dona/grafico-dona.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    Graficas1RoutingModule,
    ChartsModule
  ],
  declarations: [
    Graficas1Component,
    GraficoDonaComponent
  ]
})
export class Graficas1Module { }
