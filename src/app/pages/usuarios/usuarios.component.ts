import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/service.index';
import { Usuario } from '../../models/usuario.model';

import swal from 'sweetalert2';
import { ProfileComponent } from '../profile/profile.component';
import { SubirArchivoService } from '../../services/subir-archivo/subir-archivo.service';
import { ModalUploadService } from '../../components/modal-upload/modal-upload.service';

declare var $: any;


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styles: []
})
export class UsuariosComponent implements OnInit {

  // Imagen de usuairo
  imagenSubir: File;
  imagenTemp: string;

  // ----------------

  usuario: Usuario[] = [];
  desde: number = 0;
  totalRegistros: number = 5;
  paginacion = 20;
  page = 1;
  cargando: boolean = false;

  constructor(
    public _usuarioService: UsuarioService,
    public _cargaArchivoService: SubirArchivoService,
    public _modalUploadService: ModalUploadService,
  ) {
  }

  ngOnInit() {
    this.getUsers();
    this._modalUploadService.notificacion
      .subscribe(resp => this.getUsers())
  }

  getUsers() {
    // console.log('cargando usuarios ...')
    this.cargando = true;
    this._usuarioService.cargarUsuarios(this.desde)
      .subscribe((resp: any) => {
        this.usuario = resp.usuarios;
        this.totalRegistros = resp.total;
        this.paginacion = (this.totalRegistros / 5) * 10;
      })
  }

  next(valor: number) {
    valor -= 1;
    valor = valor * 5;
    if (valor <= 0) {
      this.desde = 0;
      this.getUsers();
    } else {
      this.desde = valor;
      this.getUsers();
    }
  }

  searchUser(termino: string) {
    this.cargando = true;
    if (termino.length <= 0) {
      this.getUsers();
      this.cargando = true;
      return;
    }
    this._usuarioService.searchUser(termino)
      .subscribe((resp: any) => {
        this.totalRegistros = resp.usuario.length;
        this.usuario = resp.usuario;
        this.cargando = true;
        this.paginacion = (this.totalRegistros / 5) * 10;

      });
    this.cargando = false;
  }
  /**
  * Deleted de Isuario
  */
  deletedUser(usuario: Usuario) {
    console.log('Usuario Eliminado' + usuario._id)
    if (usuario._id === this._usuarioService.usuario._id) {
      console.log('El usuario logueado no puede ser eliminado');
      swal('No se puede borrar usuario', 'No se puede borrar asi mismo', 'error');
      return;
    }
    swal({
      title: 'Estas seguro?',
      text: 'Esta apunto de borrar a ' + usuario.nombre,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, borrar'
    }).then((result) => {
      if (result.value) {
        this._usuarioService.deleteUser(usuario._id)
          .subscribe(resp => {
            this.getUsers();
          })

      }
    })
  }

  /**
  * Actualizar rol del usuario
  */
  updateUser(usuario: Usuario) {
    console.log(usuario);
    this._usuarioService.updatedUser(usuario, usuario._id)
      .subscribe(resp => {
        console.log('Edicion de usuario')
      })
  }
  viewModal(id: string) {
    console.log(id)
    this._modalUploadService.viewModal('usuarios', id);
  }
}
