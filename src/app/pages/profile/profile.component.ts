import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../models/usuario.model';
import { UsuarioService } from '../../services/usuario/usuario.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styles: []
})
export class ProfileComponent implements OnInit {

  usuario: Usuario;
  imagenSubir: File;
  imagenTemp: string | ArrayBuffer;
  constructor(
    public _usuarioService: UsuarioService
  ) {
    this.usuario = this._usuarioService.usuario;
  }

  ngOnInit() {
  }

  updateUser(usuario: Usuario) {
    this.usuario.nombre = usuario.nombre;

    if ( !this.usuario.google ) {
      this.usuario.email = usuario.email;
    }
    this._usuarioService.updatedUser( this.usuario , this.usuario._id)
      .subscribe( resp => {
        // console.log(resp);
      })
  }
  /**
  * Evento se dispara cada que seleciona una imagen
  * recive un archivo tipo file
  */
  selectionImagen( archivo: any) {
    if ( !archivo) {
      this.imagenSubir = null;
      return;
    }
    if (archivo.type.indexOf('image') < 0 ) {
      swal('Solo imágenes', 'El archivo seleccionado no es una imagen', 'error');
      this.imagenSubir = null;
    }
    this.imagenSubir = archivo;

    let reader = new FileReader();

    let urlImgenTemp = reader.readAsDataURL(archivo);

    reader.onloadend = () => {
      this.imagenTemp = reader.result;
      console.log( reader.result)
    }
    console.log(archivo)
  }

  changeImage() {
    this._usuarioService.changeImage(this.imagenSubir , this.usuario._id)
  }
}
