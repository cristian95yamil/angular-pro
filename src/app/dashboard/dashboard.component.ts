import { Component , OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SettingsService } from '../services/settings/settings.service';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {

  tema: any;
  constructor(public _service: SettingsService ) { }

  ngOnInit() {
    this._service.cargarAjustes();
  }

}
