import { Component, OnInit } from '@angular/core';
import { SidebarService, UsuarioService } from '../services/service.index';
import { Usuario } from '../models/usuario.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html'
})
export class FullLayoutComponent implements OnInit {

  disabled = false;
  status: {isopen: boolean} = {isopen: false};
  usuario: Usuario;
  constructor(
    public _service: SidebarService,
    public _usuarioService: UsuarioService,
    public router: Router) {
  }

  ngOnInit() {
    this.usuario = this._usuarioService.usuario;
    this._service.cargarMenu();
  }
  public toggled(open: boolean): void {
    console.log('Dropdown is now: ', open);
  }

  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }
  search(busqueda: string) {
    this.router.navigate(['/menu/busqueda', busqueda])
  }
}
