import { Pipe, PipeTransform } from '@angular/core';
import { URL_SERVICIOS } from '../config/config';

@Pipe({
  name: 'imagen'
})
export class ImagenPipe implements PipeTransform {

  transform(img: string, tipo: string , args?: any): any {

    let url = URL_SERVICIOS + '/img';

    if ( !img ) {
      return url + '/usuarios/xxaas'
    }
    if (img.indexOf('https') >= 0) {
      console.log(img)
      // imagen de google
      return img;
    }
    switch ( tipo ) {
      case 'usuarios':
         url += '/usuarios/' + img;
        break;
      case 'medicos':
         url +=  '/medicos/' + img;
        break;
      case 'hospitales':
        url += '/hospitales/' + img;
        break;
      default :
        console.log('Tipo de imagen no existe ' + tipo);
        return url + '/usuarios/xxaas'
    }
    return url;
  }

}
