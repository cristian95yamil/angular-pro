import { Usuario } from './usuario.model';
import { Hospital } from './hospital.model';


/**
 * Modelo de Medico
 */
export class Medico {
    constructor(
        public _id?: string,
        public nombre?: string,
        public usuario?: string,
        public hospital?: Hospital,
        public img?: string,
    ) {}
}
