/**
 * Modelo de Hospital
 */
export class Hospital {
    constructor(
        public _id: string,
        public nombre?: string,
        public img?: string,
    ) {}
}
