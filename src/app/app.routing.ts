import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './login/register.component';
import { NopagefoundComponent } from './shared/nopagefound/nopagefound.component';
import { LoginGuardGuard } from './services/guards/login-guard.guard';

export const routes: Routes = [
  {path: '', redirectTo: 'menu', pathMatch: 'full'},
  {path: 'menu', redirectTo: 'menu', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},


  // {path: '**', component: NopagefoundComponent},
  {
    path: '',
    canActivate: [LoginGuardGuard],
    component: FullLayoutComponent, data: {
      title: 'Menu'
    },
    children: [
      // {
      //   path: 'dashboard',
      //   loadChildren: './dashboard/dashboard.module#DashboardModule'
      // },
      {
        path: 'menu',
        loadChildren: './pages/pages.module#PagesModule'
      },
      // {
      //   path: 'progress',
      //   loadChildren: './views/progress/progress.module#ProgressModule'
      // },
      {
        path: 'graficas',
        loadChildren: './pages/graficas1/graficas1.module#Graficas1Module'
      },
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
