import { Component , OnInit } from '@angular/core';
import { SettingsService } from './services/settings/settings.service';

@Component({
  // tslint:disable-next-line
  selector: 'body',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent {
  constructor(public _service: SettingsService) {
  }
  OnInit() {
    this._service.cargarAjustes();
  }
}
