import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { URL_SERVICIOS } from '../../config/config';
import { UsuarioService } from '../usuario/usuario.service';
import swal from 'sweetalert2';
import { Hospital } from '../../models/hospital.model';

@Injectable()
export class HospitalesService {
  totalHospitales;
  constructor(
    public http: HttpClient,
    public router: Router,
    public _usuarioService: UsuarioService
  ) { }

  getHospitales(desde: number = 0) {
    let url = URL_SERVICIOS + '/hospital?desde=' + desde;

    return this.http.get( url )
      .map( (resp: any) => {
        this.totalHospitales = resp.total;
        return resp.hospitales;
      } ) ;
  }
  searchHospital( id: string) {
    let url = URL_SERVICIOS + '/busqueda/coleccion/hospital/' + id;
    return this.http.get(url)
      .map( (resp: any) => resp.hospital );
  }
  deleteHospital( id: string) {
    let url = URL_SERVICIOS + '/hospital/' + id;
    url += '?token=' + this._usuarioService.token;
    return this.http.delete( url)
      .map( (resp: any) => {
        swal(
          'El Hospital se ha eliminado correctamente',
          'Exito',
          'success'
        )
      } )
  }
  newHospital(nombre: string) {
    let url = URL_SERVICIOS + '/hospital/';
    url += '?token=' + this._usuarioService.token;
    return this.http.post( url , {nombre } )
      .map( (resp: any) => resp.hospital )
  }
  getHospital( id: string) {
    let url = URL_SERVICIOS + '/hospital/' + id;
    return this.http.get(url)
      .map( (resp: any) => {
        return resp.hospital;
      })
  }

  updateHospital(hospital: Hospital) {
    let url = URL_SERVICIOS + '/hospital/' + hospital._id;
    url += '?token=' + this._usuarioService.token;
    return this.http.put( url , hospital)
      .map ( (resp: any) => {
        swal(
          'El Hospital se ha actualizado correctamente',
          'Exito',
          'success'
        )
        return resp.hospital;
      }
    )}
}
