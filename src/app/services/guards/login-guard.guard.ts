import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UsuarioService } from '../usuario/usuario.service';
import swal from 'sweetalert2';

@Injectable()
export class LoginGuardGuard implements CanActivate {

  constructor(public _usuarioService: UsuarioService,
  public router: Router) {
  }
  canActivate() {
    if ( this._usuarioService.isLogueado()) {
      return true;
    }
    console.log( 'No esta logueado no paso el guard')
    swal({
      position: 'top-end',
      type: 'error',
      title: 'Primero debe loguearse para ingresar al menu',
      showConfirmButton: false,
      timer: 1800
    })
    this.router.navigate(['/login']);
    return false;
  }
}
