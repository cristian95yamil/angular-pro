import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UsuarioService } from '../usuario/usuario.service';

@Injectable()
export class AdminGuard implements CanActivate {
  constructor(
    public _usurioService: UsuarioService,
    public router: Router
   ) {

  }
  canActivate() {
    if (this._usurioService.usuario.role === 'ADMIN_ROLE') {
      return true;
    }else {
      console.log('ruta bloqueada por el admin role');
      this._usurioService.logout();
      return false;
    }
  }
}
