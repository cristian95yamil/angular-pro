import { Injectable } from '@angular/core';
import { URL_SERVICIOS } from '../../config/config';
import swal from 'sweetalert2';


@Injectable()
export class SubirArchivoService {

  constructor() { }

  uploadFile(archivo: File, tipo: string, id: string) {

    return new Promise((resolve, reject) => {
      let formData = new FormData();
      let xhr = new XMLHttpRequest();

      // imagen : fileSeleccionado para subir
      formData.append('imagen', archivo, archivo.name);

      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) { // si el estado cuando haya terminado es 4
          if (xhr.status === 200) { // devuelve 200 si la operacion salir correcta
            console.log('Imagen subida correctamente');
            swal({
              position: 'top-end',
              type: 'success',
              title: 'Imagen actualizada correctamente',
              showConfirmButton: false,
              timer: 1500
            })
            resolve(JSON.parse(xhr.response)); // Devuelve el usuario
          } else {
            console.log('Fallo la subida');
            reject(xhr.response);
          }
        }
      }
      let url = URL_SERVICIOS + '/upload/' + tipo + '/' + id;

      xhr.open('PUT', url, true);
      xhr.send(formData);
    });

  }
}
