import { Injectable } from '@angular/core';
import { UsuarioService } from '../usuario/usuario.service';

@Injectable()
export class SidebarService {

  menu: any = [];
  constructor(
    public _usuarioService: UsuarioService
  ) {
  }

  cargarMenu() {
    this.menu = this._usuarioService.menu;
    if ( this.menu === undefined) {
      this.menu = localStorage.getItem('menu')
    }
    console.log(this.menu)
  }
}
