// serviciso de crud
export { MedicoService } from './medico/medico.service';
export { HospitalesService } from './hospital/hospitales.service';
export { UsuarioService } from './usuario/usuario.service';

// guards
export { AdminGuard } from './guards/admin.guard';
export { LoginGuardGuard } from './guards/login-guard.guard';

export { SubirArchivoService } from './subir-archivo/subir-archivo.service';
export { SettingsService } from './settings/settings.service';
export { SharedService } from './shared/shared.service';
export { SidebarService } from './sidebar/sidebar.service';
