import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';

@Injectable()
export class SettingsService {

  ajuste: Ajuste = {
    temaUrl: '',
    color: '',
    id: 'tema'
  }
  tema: any
  constructor(@Inject(DOCUMENT) private _document,
) { }

  guardarAjustes(color: string) {
    console.log('Guardado en el localStorage');
    this.ajuste.color = color ;
    localStorage.setItem('ajustes', JSON.stringify(this.ajuste));
  }

  cargarAjustes() {
    if (localStorage.getItem('ajustes')) {
      this.ajuste = JSON.parse(localStorage.getItem('ajustes'));
      this.tema = this._document.getElementById(this.ajuste.id);
      this.tema.style.background = this.ajuste.color;
      console.log('cargando del localstorage');
    }else {
      // console.log('usando valores por defecto');
    }
  }

}

interface Ajuste {
  temaUrl: string;
  color: string;
  id: string;
}
