import { Injectable } from '@angular/core';
import { URL_SERVICIOS } from '../../config/config';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { UsuarioService } from '../usuario/usuario.service';
import swal from 'sweetalert2';
import { Medico } from '../../models/medico.model';


@Injectable()
export class MedicoService {
  totalHospitales;
  constructor(
    public http: HttpClient,
    public router: Router,
    public _usuarioService: UsuarioService
  ) { }

  getMedico(desde: number = 0) {
    let url = URL_SERVICIOS + '/medico?desde=' + desde;

    return this.http.get(url)
      .map((resp: any) => {
        this.totalHospitales = resp.total;
        return resp.medicos;
      });
  }
  searchMedico(id: string) {
    let url = URL_SERVICIOS + '/busqueda/coleccion/hospital/' + id;
    return this.http.get(url)
      .map((resp: any) => resp.medicos);
  }
  deleteMedico(id: string) {
    let url = URL_SERVICIOS + '/medico/' + id;
    url += '?token=' + this._usuarioService.token;
    return this.http.delete(url)
      .map((resp: any) => {
        swal(
          'El Hospital se ha eliminado correctamente',
          'Exito',
          'success')
      })
  }
  newMedico(medico: Medico) {
    let url = URL_SERVICIOS + '/medico/';

    if (medico._id != null) {
      // actualizando el medico
      url += medico._id + '/';
      url += '?token=' + this._usuarioService.token;
      console.log(medico)
      return this.http.put(url, medico)
        .map((resp: any) => {
          swal(
            'El Medico se ha actualizado  correctamente',
            'Exito',
            'success'
          )
          return resp.medico;
        })
    } else {
      url += '?token=' + this._usuarioService.token;
      return this.http.post(url, medico)
        .map((resp: any) => {
          swal(
            'El Medico se ha creado  correctamente',
            'Exito',
            'success'
          )
          return resp.medico;
        })
    }
  }

  updateMedico(medico: Medico) {
    let url = URL_SERVICIOS + '/medico/' + medico._id;
    url += '?token=' + this._usuarioService.token;
    return this.http.put(url, medico)
      .map((resp: any) => {
        swal(
          'El Hospital se ha actualizado correctamente',
          'Exito',
          'success'
        )
        return resp.medicos;
      }
      )
  }

  cargarMedico(id: string) {
    let url = URL_SERVICIOS + '/medico/' + id;

    return this.http.get(url)
      .map((resp: any) => resp.medico)
  }
}
