import { Injectable } from '@angular/core';
import { Usuario } from '../../models/usuario.model';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICIOS } from '../../config/config';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import swal from 'sweetalert2';
import { SubirArchivoService } from '../subir-archivo/subir-archivo.service';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class UsuarioService {

  usuario: Usuario;
  token: string;
  menu: any;

  constructor(
    public http: HttpClient,
    public router: Router,
    public subirArchivo: SubirArchivoService,
  ) {
    this.loadStorage();
  }

isLogueado () {
  return ( this.token.length > 5 ) ? true : false;
}
logout() {
  this.token = '';
  this.usuario = null;
  this.menu = null;


  localStorage.removeItem('token');
  localStorage.removeItem('usuario');
  localStorage.removeItem('menu');
  this.router.navigate(['/login']);
}

loadStorage() {
  if (localStorage.getItem('token')) {
    this.token = localStorage.getItem('token');
    this.usuario = JSON.parse(localStorage.getItem('usuario'))
    this.menu = JSON.parse(localStorage.getItem('menu'))

  }else {
    this.token = '' ;
    this.usuario = null;
  }
}

saveStorage( id: string, token: string, usuario: Usuario, menu: any) {
  localStorage.setItem('id' , id);
  localStorage.setItem('token' , token);
  localStorage.setItem('usuario' , JSON.stringify(usuario) );
  localStorage.setItem('menu' , JSON.stringify(menu) );


  this.usuario = usuario;
  this.token = token;
}
/**
* Login con Google
*/

  loginWhitGoogle( token: string) {
    let url = URL_SERVICIOS + '/login/google';
    return this.http.post( url, {token: token })
      .map( (resp: any) => {
        this.saveStorage(resp.id, resp.token, resp.usuario, resp.menu);
        this.menu = resp.menu;
        return true;
      })

  }
  /**
  * Login de Usuario
  */
  login(usuario: Usuario, recordar: boolean = false) {

    if ( recordar ) {
      localStorage.setItem('email', usuario.email );
    }else {
      localStorage.removeItem('email');
    }
    let url = URL_SERVICIOS + '/login';
    return this.http.post(url, usuario)
      .map((resp: any) => {
        this.saveStorage(resp.id, resp.token, resp.usuario, resp.menu);
        this.menu = resp.menu;
        return true;
      })
      .catch ( err => {
        console.log(err.error.mensaje)
        swal({
          position: 'top-end',
          type: 'error',
          title:  err.error.mensaje,
          showConfirmButton: false,
          timer: 2000
        })
        return Observable.throw(err)
      })
  }
  /**
  * Crear Usuario
  */
  registerUser(usuario: Usuario) {
    const url = URL_SERVICIOS + '/usuario';
    return this.http.post(url, usuario)
      .map((resp: any) => {
        swal({
          position: 'top-end',
          type: 'success',
          title: 'Usuario ' + usuario.nombre + ' creado correctamente',
          showConfirmButton: false,
          timer: 2000
        })
        return resp.usuario;
      })
      .catch ( err => {
        console.log(err.error.errors.message)
        swal({
          position: 'top-end',
          type: 'error',
          title:  err.error.errors.message,
          showConfirmButton: false,
          timer: 2000
        })
        return Observable.throw(err)
      })
  }
  /**
  * Actualizar Usuario
  */
  updatedUser(usuario: Usuario , id: string) {

    let url = URL_SERVICIOS + '/usuario/' + id ;
    url += '?token=' + this.token;
    return this.http.put( url , usuario )
      .map( (resp: any) => {
        if ( this.usuario._id === usuario._id ) {
          this.usuario = resp.usuario;
          let usuarioDB: Usuario = resp.usuario;
          this.saveStorage(resp.id, resp.token, resp.usuario, resp.menu);
          this.menu = resp.menu;        }
        swal(
          'Exito!',
          'Usuario actualizado ' + usuario.nombre ,
          'success'
        )
        return true;
      });
  }

  /**
  * Actualizar  imagen
  */
 changeImage( archivo: File, id: string) {
    this.subirArchivo.uploadFile(archivo , 'usuarios', id)
        .then( (resp: any) => {
          swal(
            'Exito!',
            'Usuario actualizado correctamente' ,
            'success'
          )
          this.usuario.img = resp.usuario.img;
          this.saveStorage(resp.id, resp.token, resp.usuario, resp.menu);
          this.menu = resp.menu;        })
        .catch( resp => {
          console.log( resp)
        })
 }
 cargarUsuarios( desde: number = 0 ) {
  let url = URL_SERVICIOS + '/usuario/?desde=' + desde;

  return this.http.get( url );

 }
 /**
  * Buscar usuario
  * @param termino
  */
 searchUser(termino: string) {
  let url = URL_SERVICIOS + '/busqueda/coleccion/usuario/' + termino ;
  return this.http.get( url);
  }

  /**
   * Eliminacion de Usuario
   */
  deleteUser(id: string) {
    let url = URL_SERVICIOS + '/usuario/' + id;

    url += '?token=' + this.token;
    return this.http.delete( url )
      .map( resp => {
        console.log('Eliminando persona ...');
        swal(
          'Eliminado !',
          'El usuario ha sido eliminado correctamente.',
          'success'
        )
      })

  }
}
